package com.jus15;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.jus15.json.AsyncTaskJUS15;
import com.jus15.json.AsyncTaskJUS15.AsyncTaskPurpose;
import com.jus15.json.JSONMaker;
import com.jus15.ui.RippleView;
import com.jus15.utility.Constraint;

public class LandingScreen extends Activity implements OnClickListener {

	ImageView checkbox;

	Dialog battery_low_dialog;

	public static Context context;

	String phone;

	SharedPreferences pref;

	String checkFlag="0";
	String flags ;
	RippleView rippleView;
	String deviceId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.landing_screen);

		context = this;

		/*........register button id.......*/
		((RippleView) findViewById(R.id.rippleViewLanding)).setOnClickListener(this);
		
		
		/*..........register image view id..........*/
		checkbox = (ImageView) findViewById(R.id.Checkbox);
		Update();

		
		/*........get shared preference private mode..........*/
		pref = getApplication().getSharedPreferences("option", MODE_PRIVATE);
		
		/*.........get mobile no......*/
		phone = pref.getString("mobile", phone); 
		deviceId=pref.getString("deviceId", null);
		//end shared prefernce
		
	}

	/*.........image change.......*/
	private void Update() {

		checkbox.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (Constraint.flag) {

					checkbox.setImageResource(R.drawable.unckeck);
					Constraint.flag = false;
					checkFlag="0";

				} else {

					Constraint.flag = true;
					checkbox.setImageResource(R.drawable.checkbox_icon);
					checkFlag="1";
				
				}
				
				if(deviceId!=null && phone!=null){
				checkUpdateData();
				}
			}

		});

	}

	

	/*........hit update api for check image view............*/ 

	private void checkUpdateData() {

		/*........... calling AsyncTask class........*/
		ArrayList<HashMap> arrayJsonRequired  = new ArrayList<HashMap> ();
		
		AsyncTaskJUS15 asyncTaskClass=new AsyncTaskJUS15(context, AsyncTaskPurpose.update);
		
		HashMap<String, String> mobilemap = new HashMap<String, String>();
     	mobilemap.put("JSONKEYNAME", "mobile");
		mobilemap.put("JSONKEYNAMEVALUE",phone );
		
		HashMap<String, String> namemap = new HashMap<String, String>();
		namemap.put("JSONKEYNAME", "flag");
		namemap.put("JSONKEYNAMEVALUE", checkFlag );
		

		HashMap<String, String> devicemap=new HashMap<String,String>();
        devicemap.put("JSONKEYNAME", "device_id");
        devicemap.put("JSONKEYNAMEVALUE",deviceId);
		
		
		arrayJsonRequired.add(namemap);
		arrayJsonRequired.add(mobilemap);
		arrayJsonRequired.add(devicemap);
		
		JSONMaker jsonmaker = new JSONMaker(arrayJsonRequired);
		String jsonstring = jsonmaker.createJson(); 
		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("data", jsonstring);

		asyncTaskClass.execute(hashMap);

	}



	@Override
	public void onClick(View v) {

		startService(new Intent(context, AlarmService.class));
		
//		/*........... calling service class..........*/
//		PendingIntent piflag = PendingIntent.getService(LandingScreen.this, 0,
//				new Intent(LandingScreen.this, AlarmService.class),
//				PendingIntent.FLAG_UPDATE_CURRENT);
//
//		/*...........alarm to start service.........*/
//		AlarmManager amflag = (AlarmManager) LandingScreen.this
//				.getSystemService(Context.ALARM_SERVICE);
//		amflag.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//				SystemClock.elapsedRealtime() + Constraint.syncTime, piflag);

	}

	public void checkResult(int status, String msg) {

		if (status == 1) {

		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

		
		} else {

		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

		}
		}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.back_slide_in, R.anim.back_slide_out);
	}
}

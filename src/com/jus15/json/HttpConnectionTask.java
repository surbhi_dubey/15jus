package com.jus15.json;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class HttpConnectionTask {

	URL geturl;
	HttpURLConnection httpURLConnection;
	StringBuilder stringBuilder;
	Context context;

	public HttpConnectionTask(String url, Context context) throws IOException {

		System.setProperty("http.keepAlive", "false");
		geturl = new URL(url);
		httpURLConnection = (HttpURLConnection) geturl.openConnection();
		this.context = context;

	}

	// post method using...............
	public String fetchPostData(Map<String, String> params) {

		String returnValue = "";
		stringBuilder = new StringBuilder();

		// Prefs prefs = new Prefs();

		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, String> entry = iterator.next();
			stringBuilder.append(entry.getKey()).append('=')
					.append(entry.getValue());
			if (iterator.hasNext()) {
				stringBuilder.append('&');
			}
		}

		String body = stringBuilder.toString();

		byte[] byteData = body.getBytes();

		try {

			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setFixedLengthStreamingMode(byteData.length);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");

			/**
			 * set headers auth token ... set api key..set config version..
			 */
			httpURLConnection.setRequestProperty("API_KEY", "andapikey");

			httpURLConnection.setRequestProperty("APP_VERSION", "1.0");

			httpURLConnection.setRequestProperty("CONFIG_VERSION", "1.0");

			String strAuthToken = "";

			if (strAuthToken != null) {
				httpURLConnection
						.setRequestProperty("AUTH_TOKEN", strAuthToken);
			}

			OutputStream stream = new BufferedOutputStream(httpURLConnection.getOutputStream());
			stream.write(byteData);
			stream.close();

			
			int status = httpURLConnection.getResponseCode();

			if (status == 200) {

				/**
				 * go to assets/conf/variable_Names for shared prefernces name
				 */

				// prefs.setPreferences(context, "API_KEY",
				// "andapikey");
				// prefs.setPreferences(context, "APP_VERSION",
				// httpURLConnection.getHeaderField("APP_VERSION"));
				// prefs.setPreferences(context, "CONFIG_VERSION",
				// httpURLConnection.getHeaderField("CONFIG_VERSION"));
				// prefs.setPreferences(context, "AUTH_TOKEN",
				// httpURLConnection.getHeaderField("AUTH_TOKEN"));

			}

			InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
			BufferedReader brReader = new BufferedReader(new InputStreamReader(
					inputStream));
			String line = "";
			
			StringBuffer response = new StringBuffer();

			while ((line = brReader.readLine()) != null) {

				response.append(line);
				response.append("\r");
			}

			brReader.close();

			return returnValue = response.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return returnValue;
		}

		finally {
			if (httpURLConnection != null) {
				httpURLConnection.disconnect();
			}
		}
	}

	public String fetchGetData() {

		SharedPreferences prefs = context.getSharedPreferences("KONSULT_PREFS",
				Context.MODE_PRIVATE);

		try {

			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setRequestProperty("Content-Type",
					"application/json");

			httpURLConnection.setRequestProperty("API_KEY",
					prefs.getString("API_KEY", "andapikey"));
			httpURLConnection.setRequestProperty("APP_VERSION",
					prefs.getString("APP_VERSION", "1.0"));
			httpURLConnection.setRequestProperty("CONFIG_VERSION",
					prefs.getString("CONFIG_VERSION", "1.0"));

			String strAuthToken = prefs.getString("AUTH_TOKEN", null);

			if (strAuthToken != null) {

				httpURLConnection
						.setRequestProperty("AUTH_TOKEN", strAuthToken);
			}

			InputStream is = httpURLConnection.getInputStream();

			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			String line = "";

			StringBuffer response = new StringBuffer();

			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}

			rd.close();

			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return null;

		} finally {

			if (httpURLConnection != null) {
				httpURLConnection.disconnect();
			}
		}
	}

	public int fetchPostStatus(Map<String, String> params) {

		stringBuilder = new StringBuilder();

		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();

		// constructs the POST body using the parameters

		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			stringBuilder.append(param.getKey()).append('=')
					.append(param.getValue());
			if (iterator.hasNext()) {
				stringBuilder.append('&');
			}
		}

		String body = stringBuilder.toString();

		byte[] bytes = body.getBytes();
		try {
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setFixedLengthStreamingMode(bytes.length);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");

			// post the request
			OutputStream out = httpURLConnection.getOutputStream();
			out.write(bytes);
			out.close();

			// handle the response when we are in need to read status code only
			int status = httpURLConnection.getResponseCode();

			if (status != 200) {

				httpURLConnection.getHeaderField("APP_VERSION");
				Log.e("header tag",
						httpURLConnection.getHeaderField("APP_VERSION")
								.toString());

				return 0;
			} else if (status == 200) {
				return 1;
			}

		} catch (ProtocolException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (httpURLConnection != null) {
				httpURLConnection.disconnect();
			}
		}
		return 0;
	}

}

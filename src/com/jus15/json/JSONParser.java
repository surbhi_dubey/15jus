package com.jus15.json;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.jus15.AlarmService;
import com.jus15.LandingScreen;
import com.jus15.UserName;
import com.jus15.VerificationCode;
import com.jus15.location.GPSTracker;
import com.jus15.utility.Constraint;

public class JSONParser {

	Context context;

	GPSTracker gpsTracker = new GPSTracker();

	public JSONParser(Context context) {

		this.context = context;
	}

	// jyoti code
	public void RegistrationParser(String result) {

		try {

			if (result.trim().length() > 0) {
				// {"data":{"status":0,"msg":"User Already Exist"}}
				JSONObject jsonObject = new JSONObject(result);
				String data = jsonObject.optString("data");
				JSONObject jsondata = new JSONObject(data);
				int status = jsondata.optInt("status");
				String msg = null;
				if (status == 0) {

					msg = jsondata.optString("msg");

					String error = jsondata.optString("error");
					if (error.trim().length() > 0) {
						if (error != null) {
							JSONObject jsonerror = new JSONObject(error);

							JSONArray mobilearray = jsonerror
									.getJSONArray("mobile");
							JSONArray devicearray = jsonerror
									.getJSONArray("device_id");

							if (mobilearray != null) {

								Log.e("tag error", mobilearray.toString());

								for (int i = 0; i < mobilearray.length(); i++) {
									msg = msg + mobilearray.getString(i);
								}
							} else if (devicearray != null) {

								Log.e("tag error", devicearray.toString());

								for (int i = 0; i < devicearray.length(); i++) {
									msg = msg + devicearray.getString(i);
								}

								((VerificationCode) context).checkResult(
										status, msg);

							} else {

								msg = jsondata.optString("msg");
								((VerificationCode) context).checkResult(
										status, msg);
							}
						}
					}
				}

				((VerificationCode) context).checkResult(status, msg);
				Log.e("Update parser", jsonObject.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void UpdateParser(String result) {

		try {

			/*
			 * { "data": { "status": 1, "sync_time": 15, "collection_time": 60,
			 * "msg": "Profile Successfully Update" } }
			 */

			/*
			 * { "data": { "status": 0, "msg": "Invalid Data", "error": {
			 * "mobile": [ "The mobile field is required." ] } } }
			 */

			/*
			 * { "data": { "status": 0, "msg": "Invalid Data", "error": {
			 * "mobile": [ "The mobile field is required." ], "flag": [
			 * "The flag field is required." ] } } }
			 */
			JSONObject jsonObject = new JSONObject(result);

			String data = jsonObject.optString("data");

			JSONObject jsonData = new JSONObject(data);

			int status = jsonData.optInt("status");

			String msg;

			if (status == 0) {

				msg = jsonData.optString("msg");
				Log.e("error...........", status + "" + msg);
				String error = jsonData.optString("error");

				JSONObject jsonError = new JSONObject(error);

				JSONArray array = jsonError.getJSONArray("mobile");

				JSONArray arrayFlag = jsonError.getJSONArray("flag");

				Log.e("error...........", msg);

				if (array != null) {
					Log.e("tag error", array.toString());

					for (int i = 0; i < array.length(); i++) {

						msg = msg + array.getString(i);
					}
				} else if (arrayFlag != null) {

					Log.e("tag error", arrayFlag.toString());

					for (int i = 0; i < arrayFlag.length(); i++) {

						msg = msg + arrayFlag.getString(i);
					}

					((LandingScreen) context).checkResult(status, msg);

				} else {

					msg = jsonData.optString("msg");
					((LandingScreen) context).checkResult(status, msg);
				}
			}

			Log.e("Update parser", jsonObject.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void OrderParser(String result) {
		try {

			/*
			 * { "data": { "status": 0, "msg": "Invalid Data", "error": {
			 * "phone": [ "The phone field is required." ],
			 * "battery_percentage": [
			 * "The battery percentage field is required." ], "latitude": [
			 * "The latitude field is required." ], "longitude": [
			 * "The longitude field is required." ], "time": [
			 * "The time field is required." ] } } }
			 */

			JSONObject jsonObject = new JSONObject(result);

			String data = jsonObject.optString("data");

			JSONObject jsonData = new JSONObject(data);

			int status = jsonData.optInt("status");
			String msg;
			if (status == 0) {

				msg = jsonData.optString("msg");

				Log.e("Status of Order", "" + status);

				String error = jsonData.optString("error");

				JSONObject jsonError = new JSONObject(error);

				JSONArray arrayPhone = jsonError.getJSONArray("phone");

				JSONArray arrayBatteryPercentage = jsonError
						.getJSONArray("battery_percentage");

				JSONArray arraylatitude = jsonError.getJSONArray("latitude");

				JSONArray arraylongitude = jsonError.getJSONArray("longitude");

				JSONArray arraytime = jsonError.getJSONArray("time");

				if (arrayPhone != null) {
					Log.e("tag error", arrayPhone.toString());

					for (int i = 0; i < arrayPhone.length(); i++) {

						msg = msg + arrayPhone.getString(i);
					}
				} else if (arrayBatteryPercentage != null) {

					Log.e("tag error", arrayBatteryPercentage.toString());

					for (int i = 0; i < arrayBatteryPercentage.length(); i++) {

						msg = msg + arrayBatteryPercentage.getString(i);

					}
				} else if (arraylatitude != null) {

					Log.e("tag error", arraylatitude.toString());

					for (int i = 0; i < arraylatitude.length(); i++) {

						msg = msg + arraylatitude.getString(i);
					}
				} else if (arraylongitude != null) {

					Log.e("tag error", arraylongitude.toString());

					for (int i = 0; i < arraylongitude.length(); i++) {

						msg = msg + arraylongitude.getString(i);
					}
				} else if (arraytime != null) {

					Log.e("tag error", arraytime.toString());

					for (int i = 0; i < arraytime.length(); i++) {

						msg = msg + arraytime.getString(i);
					}
				}
				((AlarmService) context).checkResult(status, msg);

			} else {

				msg = jsonData.optString("msg");

				int syncTime = jsonData.optInt("sync_time");
				int collectTime = jsonData.optInt("collection_time");

				Constraint.syncTime = syncTime * 60 * 1000;
				Constraint.collectionTime = collectTime * 60 * 1000;

				((AlarmService) context).checkResult(status, msg);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void PhoneStatusParser(String result) {
		try {

			/*
			 * { "data": { "status": 0, "msg": "Invalid Data", "error": {
			 * "phone": [ "The phone field is required." ],
			 * "battery_percentage": [
			 * "The battery percentage field is required." ], "latitude": [
			 * "The latitude field is required." ], "longitude": [
			 * "The longitude field is required." ], "time": [
			 * "The time field is required." ] } } }
			 */

			/* {"data":{"status":0,"msg":"Invalid Account"}} */

			if (result.trim().length() > 0) {

				JSONObject jsonObject = new JSONObject(result);

				String data = jsonObject.optString("data");

				JSONObject jsonData = new JSONObject(data);

				int status = jsonData.optInt("status");
				String msg;
				if (status == 0) {

					msg = jsonData.optString("msg");

					Log.e("Status of Order", "" + status);

					String error = jsonData.optString("error");

					if (error.trim().length() > 0) {
						if (error != null) {

							JSONObject jsonError = new JSONObject(error);

							JSONArray arrayPhone = jsonError
									.getJSONArray("phone");

							JSONArray arrayBatteryPercentage = jsonError
									.getJSONArray("battery_percentage");

							JSONArray arraylatitude = jsonError
									.getJSONArray("latitude");

							JSONArray arraylongitude = jsonError
									.getJSONArray("longitude");

							JSONArray arraytime = jsonError
									.getJSONArray("time");

							if (arrayPhone != null) {
								Log.e("tag error", arrayPhone.toString());

								for (int i = 0; i < arrayPhone.length(); i++) {

									msg = msg + arrayPhone.getString(i);
								}
							} else if (arrayBatteryPercentage != null) {

								Log.e("tag error",
										arrayBatteryPercentage.toString());

								for (int i = 0; i < arrayBatteryPercentage
										.length(); i++) {

									msg = msg
											+ arrayBatteryPercentage
													.getString(i);

								}
							} else if (arraylatitude != null) {

								Log.e("tag error", arraylatitude.toString());

								for (int i = 0; i < arraylatitude.length(); i++) {

									msg = msg + arraylatitude.getString(i);
								}
							} else if (arraylongitude != null) {

								Log.e("tag error", arraylongitude.toString());

								for (int i = 0; i < arraylongitude.length(); i++) {

									msg = msg + arraylongitude.getString(i);
								}
							} else if (arraytime != null) {

								Log.e("tag error", arraytime.toString());

								for (int i = 0; i < arraytime.length(); i++) {

									msg = msg + arraytime.getString(i);
								}
							}
						}
					}

					gpsTracker.checkResult(status, msg);

				} else {
					int syncTime = jsonData.optInt("sync_time");
					int collectTime = jsonData.optInt("collection_time");

					Constraint.syncTime = syncTime * 60 * 1000;
					Constraint.collectionTime = collectTime * 60 * 1000;

					msg = jsonData.optString("msg");
					gpsTracker.checkResult(status, msg);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void MessageApi(String result) {

		try {
			// /{"status":0,"data":{"msg":" Cannot process the request before your working start hour.\n","sender_api":123}}

			/*
			 * { "data": { "status": "fail", "msg": "Invalid Data", "error": {
			 * "sender_api": [ "The sender api field is required." ] } } }
			 */

			/*
			 * { "data":{ "otp": "4809", "sender_api": 123 } }
			 */

			JSONObject jsonObject = new JSONObject(result);
			String data = jsonObject.optString("data");

			JSONObject jsonData = new JSONObject(data);

			String OTP = jsonData.optString("otp");
			if (OTP != null) {
				// String msg = jsonData.optString("sender_api");

				Log.e("Error Msg is", OTP);

				((UserName) context).checkStatus(1, OTP);

			} else {

				String msg = jsonData.getString("error");
				((UserName) context).checkStatus(0, msg);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

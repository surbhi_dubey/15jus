package com.jus15.json;

import java.io.IOException;
import java.util.HashMap;

import android.content.Context;
import android.os.AsyncTask;

public class AsyncTaskJUS15 extends	AsyncTask<HashMap<String, String>, String, String> {

	Context context;
	AsyncTaskPurpose asyncTaskPurpose;

	public enum AsyncTaskPurpose {

		pushDataToServer, registration, update, order_api, phoneStatus,msg_api;
	}

	public AsyncTaskJUS15(Context context, AsyncTaskPurpose asyncTaskPurpose) {

		this.context = context;
		this.asyncTaskPurpose = asyncTaskPurpose;
	}

	@Override
	protected String doInBackground(HashMap<String, String>... params) {

		try {

			if (asyncTaskPurpose == AsyncTaskPurpose.registration) {

				return doRegistration(params[0]);

			} else if (asyncTaskPurpose == AsyncTaskPurpose.update) {

				return doUpdate(params[0]);

			} else if (asyncTaskPurpose == AsyncTaskPurpose.order_api) {

				return doOrderApi(params[0]);

			} else if (asyncTaskPurpose == AsyncTaskPurpose.phoneStatus) {

				return doPhoneStatus(params[0]);
				
			}else if(asyncTaskPurpose==AsyncTaskPurpose.msg_api){
				
				return doMessageApi(params[0]);
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;
	}

	

	@Override
	protected void onPostExecute(String result) {

		super.onPostExecute(result);

		JSONParser jsonParser = new JSONParser(context);

		if (asyncTaskPurpose == AsyncTaskPurpose.registration) {

			jsonParser.RegistrationParser(result);

		} else if (asyncTaskPurpose == AsyncTaskPurpose.update) {

			jsonParser.UpdateParser(result);

		} else if (asyncTaskPurpose == AsyncTaskPurpose.order_api) {

			jsonParser.OrderParser(result);
			
		}else if(asyncTaskPurpose==AsyncTaskPurpose.phoneStatus){
			
			jsonParser.PhoneStatusParser(result);
			
		}else if(asyncTaskPurpose==AsyncTaskPurpose.msg_api){
			
			jsonParser.MessageApi(result);
		}
	}

	private String doRegistration(HashMap<String, String> hashMap)
			throws IOException {

		String registration_url = "http://15jusapi.dev.app-labs.co/user/registration";

		HttpConnectionTask httpConnection = new HttpConnectionTask(
				registration_url, context);

		return httpConnection.fetchPostData(hashMap);

	}

	private String doUpdate(HashMap<String, String> hashMap) throws IOException {

		String update_url = "http://15jusapi.dev.app-labs.co/user/updateprofile";

		HttpConnectionTask httpConnection = new HttpConnectionTask(update_url,
				context);

		return httpConnection.fetchPostData(hashMap);
	}

	private String doOrderApi(HashMap<String, String> hashMap)
			throws IOException {

		String order_api_url = "http://15jusapi.dev.app-labs.co/orderapi";

		HttpConnectionTask httpConnection = new HttpConnectionTask(
				order_api_url, context);

		return httpConnection.fetchPostData(hashMap);
	}

	private String doPhoneStatus(HashMap<String, String> hashMap) throws IOException {
	
		String phone_status_url = "http://15jusapi.dev.app-labs.co/addphonestatus";

		HttpConnectionTask httpConnection = new HttpConnectionTask(
				phone_status_url, context);

		return httpConnection.fetchPostData(hashMap);
		
	}
	
	private String doMessageApi(HashMap<String, String> hashMap) throws IOException {
		
		String msg_api_url = "http://15jusapi.dev.app-labs.co/smsapi";

		HttpConnectionTask httpConnection = new HttpConnectionTask(
				msg_api_url, context);

		return httpConnection.fetchPostData(hashMap);
	}

}

package com.jus15.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONMakerArray {

	ArrayList<HashMap> arrayJsonRequired;
	JSONArray jsonObjectArray;
	JSONArray jsonArray;

	public JSONMakerArray(ArrayList<HashMap> arrayList) {
		this.arrayJsonRequired = arrayList;
		jsonArray = new JSONArray();
	}

	// making json........
	public String createJson() {

		JSONObject jsonObject2 = new JSONObject();
		for (int i = 0; i < arrayJsonRequired.size(); i++) {

			try {
				@SuppressWarnings("unchecked")
				HashMap<String, String> hashMapToMakeJson = arrayJsonRequired
						.get(i);
				jsonObject2.put(hashMapToMakeJson.get("JSONKEYNAME"),
						hashMapToMakeJson.get("JSONKEYNAMEVALUE"));
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		 jsonArray.put(jsonObject2);

		return jsonArray.toString();
	}

}

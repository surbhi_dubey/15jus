package com.jus15;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.jus15.database.DbHelper;
import com.jus15.json.AsyncTaskJUS15;
import com.jus15.json.AsyncTaskJUS15.AsyncTaskPurpose;
import com.jus15.json.JSONMaker;
import com.jus15.location.GPSTracker;
import com.jus15.ui.RippleView;
import com.jus15.utility.Constraint;

public class AlarmService extends Service {

	String battery_percentage;

	Context context;

	DbHelper db;

	@Override
	public void onCreate() {

		super.onCreate();

		context = this;
		registerReciever(getApplicationContext());
	}

	@Override
	public IBinder onBind(Intent intent) {

		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Context context = getApplicationContext();

		PendingIntent intent2 = PendingIntent.getService(
				getApplicationContext(), 0, new Intent(getApplicationContext(),
						AlarmService.class), PendingIntent.FLAG_UPDATE_CURRENT);

		Log.e("Service Started", Constraint.syncTime + " START");

		AlarmManager alarmManager = (AlarmManager) getApplicationContext()
				.getSystemService(Context.ALARM_SERVICE);

		alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime() + Constraint.syncTime, intent2);

		GPSTracker gpsTracker = new GPSTracker();
		gpsTracker.getLocation(context);

		return super.onStartCommand(intent, flags, startId);
	}

	// battery low dialog......
	private void BatteryLowDialog(final Context context) {

		this.context = context;
		final Dialog battery_low_dialog = new Dialog(context);

		battery_low_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		battery_low_dialog.getWindow().setType(
				WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		battery_low_dialog.setContentView(R.layout.battery_low);

		RippleView btn_yes = (RippleView) battery_low_dialog
				.findViewById(R.id.rippleViewPopUp);

		btn_yes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Order(context);
				battery_low_dialog.dismiss();
			}

		});

		TextView maybe_later = (TextView) battery_low_dialog
				.findViewById(R.id.maybe);

		maybe_later.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				battery_low_dialog.dismiss();
			}
		});

		battery_low_dialog.show();

	}

	private void registerReciever(Context context) {
		IntentFilter intentFilter = new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED);
		context.registerReceiver(battery_status, intentFilter);
	}

	private BroadcastReceiver battery_status = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			int rawlevel = intent.getIntExtra("level", -1);
			int scale = intent.getIntExtra("scale", -1);

			Constraint.battery_level = (rawlevel * 100) / scale;
			battery_percentage = Integer.toString(Constraint.battery_level);

			/* ...popup dialog.... */
			if (Constraint.battery_level == 15) {
				BatteryLowDialog(context);
			}
		}
	};

	private void Order(Context context) {

		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"option", MODE_PRIVATE);
		String device_id = pref.getString("deviceId", null);
		String phone = pref.getString("mobile", null);

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String time = dateFormat.format(calendar.getTime());

		GPSTracker gpsTracker = new GPSTracker();
		Location location = gpsTracker.getLocation(context);

		String latitude = Double.toString(location.getLatitude());
		String longitude = Double.toString(location.getLongitude());

		// Calling Asynctask Task Class...............
		ArrayList<HashMap> arrayJsonRequired = new ArrayList<HashMap>();

		AsyncTaskJUS15 asyncTaskClass = new AsyncTaskJUS15(context,
				AsyncTaskPurpose.order_api);
		HashMap<String, String> phonemap = new HashMap<String, String>();

		phonemap.put("JSONKEYNAME", "phone");
		phonemap.put("JSONKEYNAMEVALUE", phone);

		HashMap<String, String> batterymap = new HashMap<String, String>();

		batterymap.put("JSONKEYNAME", "battery_percentage");
		batterymap.put("JSONKEYNAMEVALUE", battery_percentage);

		HashMap<String, String> latitudemap = new HashMap<String, String>();

		latitudemap.put("JSONKEYNAME", "latitude");
		latitudemap.put("JSONKEYNAMEVALUE", latitude);

		HashMap<String, String> longitudemap = new HashMap<String, String>();

		longitudemap.put("JSONKEYNAME", "longitude");
		longitudemap.put("JSONKEYNAMEVALUE", longitude);

		HashMap<String, String> timemap = new HashMap<String, String>();

		timemap.put("JSONKEYNAME", "time");
		timemap.put("JSONKEYNAMEVALUE", time);

		HashMap<String, String> emailMap = new HashMap<String, String>();

		emailMap.put("JSONKEYNAME", "email");
		emailMap.put("JSONKEYNAMEVALUE", "abc@abc.com");

		HashMap<String, String> devicemap = new HashMap<String, String>();
		devicemap.put("JSONKEYNAME", "device_id");
		devicemap.put("JSONKEYNAMEVALUE", device_id);

		arrayJsonRequired.add(phonemap);
		arrayJsonRequired.add(batterymap);
		arrayJsonRequired.add(latitudemap);
		arrayJsonRequired.add(longitudemap);
		arrayJsonRequired.add(timemap);
		arrayJsonRequired.add(emailMap);
		arrayJsonRequired.add(devicemap);

		JSONMaker jsonmaker = new JSONMaker(arrayJsonRequired);
		String jsonstring = jsonmaker.createJson();
		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("status", jsonstring);

		if (phone != null && latitude != null && longitude != null
				&& timemap != null && device_id != null) {
			asyncTaskClass.execute(hashMap);
		} else {
			Log.e("AlarmService order", "Some value missing in order api");
		}

	}

	public void checkResult(int status, String msg) {

		if (status == 1) {

			Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

		} else {

			Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

		}
	}
}

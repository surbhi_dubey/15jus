package com.jus15.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
	

	private static final String DB_NAME = "JUS15.sqlite";
	private static final int DATABASE_VERSION = 1;

	public static String ID = "id";

	public static String time = "time";
	public static String user_percentage = "user_percentage";

	public static String latitude = "latitude";
	public static String longitude = "longitude";
	public static String User_Name = "name";

	static SQLiteDatabase db;
	Context context;
	private String DB_FULL_PATH = "";
	private static String DB_PATH;

	public static String status = "status";

	public DbHelper(Context context) {

		super(context, DB_NAME, null, DATABASE_VERSION);
		this.context = context;

		DB_PATH = "/data/data/" + this.context.getPackageName() + "/databases/";
		DB_FULL_PATH = DB_PATH + DB_NAME;
		Log.v("DB PATH", DB_FULL_PATH);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public void createDb(boolean versionChange) {
		boolean chkverres;
		chkverres = checkversion();
		Log.v("result of version change", "" + chkverres);
		if (!checkDb() || chkverres) {
			// Not Exist. So we have to copy the database
			copyDataBase();

		}

	}

	private void copyDataBase() {
		InputStream in;
		OutputStream os;
		byte arrByte[] = new byte[1024];

		try {
			in = context.getAssets().open(DB_NAME);

			// Making directory
			File dbFolder = new File(DB_PATH);
			if (!dbFolder.exists())
				dbFolder.mkdir();

			os = new FileOutputStream(DB_PATH + DB_NAME);
			int length;

			while ((length = in.read(arrByte)) > 0) {
				os.write(arrByte, 0, length);
			}

			// Closing the streams
			os.flush();
			in.close();
			os.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Checking the database
	private boolean checkDb() {
		db = null;

		try {
			db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
					SQLiteDatabase.OPEN_READWRITE
							| SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (db != null) {
			db.close();
		}

		return (db == null) ? false : true;
	}

	private boolean checkversion() {
		String MISC_PREFS = "MiscPrefsFile";
		String Versionname, currentVersion;
		// Checking for database existence.
		PackageManager manager = this.context.getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(
					this.context.getPackageName(), 0);
			Versionname = info.versionName;
			// Log.v("Version name", Versionname);
			SharedPreferences miscPrefs = context.getSharedPreferences(
					MISC_PREFS, Context.MODE_PRIVATE);
			currentVersion = miscPrefs.getString("Current Version", null);
			Log.v("getting version name", "getAppVersionToPrefs: got "
					+ currentVersion);
			miscPrefs.edit

			().putString("Current Version", Versionname).commit();
			Log.v("settinf version name",
					"setAppVersionToPrefs: set app version to prefs"
							+ Versionname);
			if (Versionname.equals(currentVersion)) {
				return false;
			} else {
				return true;
			}

		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;

		}

	}

	// Opening the Database.....................
	public static SQLiteDatabase openDataBase() throws SQLException {
		try {
			if (db == null) {
				db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
						SQLiteDatabase.OPEN_READWRITE
								| SQLiteDatabase.CREATE_IF_NECESSARY
								| SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			} else if (!db.isOpen()) {
				db = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null,
						SQLiteDatabase.OPEN_READWRITE
								| SQLiteDatabase.CREATE_IF_NECESSARY
								| SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return db;
	}

	public static String decodebase64(String result) {
		String response = "";
		String r1 = "";
		try {
			byte[] data = Base64.decode(result, Base64.DEFAULT);
			response = new String(data, "UTF-8");
			String replacestring = "\\" + "\"";
			response = response.replace(replacestring, "\"");

		} catch (Exception ex) {
			ex.toString();
		}
		return response;

	}

	// /Insert in database........................

	public void addData(String phone, String batteryPercent, String lattitude,
			String longitude, String time, int flag, String deviceId)
			throws IOException {

		SQLiteDatabase db = this.getWritableDatabase();
		// db = openDataBase();

		ContentValues contentvalues = new ContentValues();
		contentvalues.put("phone", phone);
		contentvalues.put("battery_percentage", batteryPercent);
		contentvalues.put("latitude", lattitude);
		contentvalues.put("longitude", longitude);
		contentvalues.put("time", time);
		contentvalues.put("status", flag);
		contentvalues.put("device_id", deviceId);
		// contentvalues.put("id", data.id);

		db.insert("log_table", null, contentvalues);

		db.close();

	}

	// Reading all Content

	public static String fetchOfflineData() {

		JSONArray jsonArray = new JSONArray();

		JSONObject jsonObject = new JSONObject();

		String query = "Select * from log_table";

		db = openDataBase();

		Cursor cursor = null;

		try {
			cursor = db.rawQuery(query, null);

			while (cursor.moveToNext()) {
				Data data = new Data();

				jsonObject.put("phone",
						cursor.getString(cursor.getColumnIndex("phone")));

				jsonObject.put("battery_percentage", cursor.getString(cursor
						.getColumnIndex("battery_percentage")));

				jsonObject.put("latitude",
						cursor.getString(cursor.getColumnIndex("latitude")));

				jsonObject.put("longitude",
						cursor.getString(cursor.getColumnIndex("longitude")));

				jsonObject.put("time",
						cursor.getString(cursor.getColumnIndex("time")));

				jsonObject.put("device_id",
						cursor.getString(cursor.getColumnIndex("device_id")));

				data.status = cursor.getInt(cursor.getColumnIndex("status"));

				jsonArray.put(jsonObject);

			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			if (db != null)
				db.close();

			if (cursor != null)
				cursor.close();
		}

		return jsonArray.toString();

	}

	/*
	 * update flag value with all session_id which flag is
	 * 0....................................................
	 */
	public static void updateAllFlag(String phone, int flag) {

		try {
			db = openDataBase();

			db.execSQL("UPDATE log_table SET status=" + flag + " WHERE phone="
					+ phone);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/*
	 * delete the record of status 1
	 */
	// Delete Row............................
	public void deletecontact() {

		try {
			SQLiteDatabase db = this.getWritableDatabase();
			db = openDataBase();

			db.execSQL("delete from log_table where status = +"+"0");

			db.close();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

}

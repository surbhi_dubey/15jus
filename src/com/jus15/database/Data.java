package com.jus15.database;

public class Data {
	// int user_phone,user_battery,flag;
	// double lat,longitude;
	String battery_percentage, phone, latitude, longitude, time;
	int id, status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getbattery_percentage() {
		return battery_percentage;
	}

	public void setbattery_percentage(String battery_percentage) {
		this.battery_percentage = battery_percentage;
	}

	public String getphone() {
		return phone;
	}

	public void setphone(String phone) {
		this.phone = phone;
	}

	public String getlatitude() {
		return latitude;
	}

	public void setlatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String gettime() {
		return time;
	}

	public void settime(String time) {
		this.time = time;
	}

	public Data(String phone, String latitude, String longitude,
			String battery_percentage, String time, int status) {
		super();
		this.phone = phone;
		this.latitude = latitude;
		this.longitude = longitude;
		this.battery_percentage = battery_percentage;
		this.time = time;

		this.longitude = longitude;
		this.status = status;

	}

	public Data() {
		// TODO Auto-generated constructor stub
	}

}

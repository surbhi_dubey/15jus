package com.jus15;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.jus15.ui.RippleView;

@SuppressLint("NewApi")
public class MobileNumber extends Activity implements OnClickListener {
	

	EditText editText_mobile_number;

	String device_id, userMobileNo;

	ProgressDialog progressDialog;

	Context mobilecontext;
	
	ImageView imageProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.mobile_number);
		
		imageProgress = (ImageView) findViewById(R.id.progressBar1);
		
		
		SharedPreferences pref = getApplicationContext().getSharedPreferences("option", MODE_PRIVATE);

		String userMobile=pref.getString("mobile", userMobileNo);

		if(userMobile!=null){
		
		Intent intent=new Intent(getApplicationContext(),LandingScreen.class);
		startActivity(intent);
		MobileNumber.this.finish();
		}else{

		mobilecontext = this;

		/* ......... get button id......... */
		((RippleView) findViewById(R.id.rippleViewMobileNO)).setOnClickListener(this);
	

		/* ..........get edittext id........... */
		editText_mobile_number = (EditText) findViewById(R.id.mobile_number);
		
		editText_mobile_number.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

				if (0 != editText_mobile_number.getText().length()) {
					String spnId = editText_mobile_number.getText().toString();
					checkProgress(spnId);
				}

			}
		});

}
	}
	
	
	
	

	@Override
	public void onClick(View v) {

		userMobileNo = editText_mobile_number.getText().toString();


		/* .............mobile no validation............ */
		if ((!userMobileNo.equals("")) && (userMobileNo.length() > 9)) {

			/* ...........start intent........... */
			Intent intent = new Intent(MobileNumber.this, UserName.class);
			intent.putExtra("MobileNo", userMobileNo);
			startActivity(intent);
			
			overridePendingTransition(R.anim.slide_in, R.anim.slide_out);		

		} else {

			Toast.makeText(MobileNumber.this,
					"Please enter a valid mobile number.", Toast.LENGTH_SHORT)
					.show();
		}

	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.back_slide_in, R.anim.back_slide_out);
	}
	
	
	void checkProgress(final String str) {
		
		if (str.trim().length() == 1) {

			imageProgress.setBackgroundResource(R.drawable.a1);
		}
		if (str.trim().length() == 2) {

			imageProgress.setBackgroundResource(R.drawable.a2);
		}
		if (str.trim().length() == 3) {

			imageProgress.setBackgroundResource(R.drawable.a3);
		}
		if (str.trim().length() == 4) {

			imageProgress.setBackgroundResource(R.drawable.a4);
		}
		if (str.trim().length() == 5) {

			imageProgress.setBackgroundResource(R.drawable.a5);
		}
		if (str.trim().length() == 6) {

			imageProgress.setBackgroundResource(R.drawable.a6);
		}
		if (str.trim().length() == 7) {

			imageProgress.setBackgroundResource(R.drawable.a7);
		}

		if (str.trim().length() == 8) {

			imageProgress.setBackgroundResource(R.drawable.a8);
		}

		if (str.trim().length() == 9) {

			imageProgress.setBackgroundResource(R.drawable.a9);
		}

		if (str.trim().length() == 10) {

			imageProgress.setBackgroundResource(R.drawable.a10);

		}

	}


}
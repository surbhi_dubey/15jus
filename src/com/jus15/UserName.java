package com.jus15;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jus15.json.AsyncTaskJUS15;
import com.jus15.json.AsyncTaskJUS15.AsyncTaskPurpose;
import com.jus15.json.JSONMaker;
import com.jus15.ui.RippleView;
import com.jus15.utility.Utilz;

public class UserName extends Activity implements OnClickListener {

	Button btn_continue;

	EditText editText_user_name;

	Context context;

	String phone, userName, senderApi="123";
	
	ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.user_name);

		context = this;
		
		Intent intent=getIntent();
		Bundle bundle=intent.getExtras();
		phone=bundle.getString("MobileNo");

		/*..........get button id........*/
		((RippleView) findViewById(R.id.rippleViewUser)).setOnClickListener(this);
		

		/*............get edit text id..........*/
		editText_user_name = (EditText) findViewById(R.id.user_name);


	}

	@Override
	public void onClick(View v) {

		userName = editText_user_name.getText().toString();
		
		Utilz utilz=new Utilz();
		
		if(userName!=null && phone!=null && senderApi!=null){

			if(utilz.isInternetConnected(context)){
			MessageApi();
			}else {
				Toast.makeText(context, "Oops! Unable to connect to the remote server.\nPlease check your internet connection.", Toast.LENGTH_SHORT).show();
			}
		}
		else{
			Toast.makeText(getApplicationContext(), "Oops! Something is not right here.\nPlease cross check the input you have given.", Toast.LENGTH_SHORT).show();
		}
		
	}

	/*...........hit massege api..........*/
	private void MessageApi() {

		
//		dialog=new ProgressDialog(context);
//		dialog.show();
		
		
		ArrayList<HashMap> arrayJsonRequired = new ArrayList<HashMap>();

		/*.........calling async task class.........*/
		AsyncTaskJUS15 asyncTaskClass = new AsyncTaskJUS15(context,
				AsyncTaskPurpose.msg_api);

		HashMap<String, String> namemap = new HashMap<String, String>();

		namemap.put("JSONKEYNAME", "sender_api");
		namemap.put("JSONKEYNAMEVALUE", senderApi);

		HashMap<String, String> mobilemap = new HashMap<String, String>();

		mobilemap.put("JSONKEYNAME", "mobile");
		mobilemap.put("JSONKEYNAMEVALUE", phone);

		arrayJsonRequired.add(namemap);
		arrayJsonRequired.add(mobilemap);

		JSONMaker jsonmaker = new JSONMaker(arrayJsonRequired);
		String jsonstring = jsonmaker.createJson();
		
		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("data", jsonstring);

		asyncTaskClass.execute(hashMap);

	}
	
	
	public void  checkStatus(int status, String msg) {
		
	//	dialog.dismiss();
		if(status==1){
			/*.......start intent.......*/
			Intent intent = new Intent(UserName.this, VerificationCode.class);
			//intent.putExtra("OTP", msg);
			intent.putExtra("OTP", "1661");
			intent.putExtra("MobileNo", phone);
			intent.putExtra("UserName", userName);
			
			startActivity(intent);
			overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			
		}else{
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.back_slide_in, R.anim.back_slide_out);
	}

	
	
}

package com.jus15.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class IncomingSms  extends BroadcastReceiver{

	// Get the object of SmsManager
	final SmsManager sms = SmsManager.getDefault();
	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {

		// Retrives a map of extended data from the intent
		final Bundle bundle = intent.getExtras();

		try {

			if (bundle != null) {

				final Object[] pdusObject = (Object[]) bundle.get("pdus");

				for (int i = 0; i < pdusObject.length; i++) {

					SmsMessage smsMessage = SmsMessage
							.createFromPdu((byte[]) pdusObject[i]);
					String phoneNumber = smsMessage
							.getDisplayOriginatingAddress();

					String senderNumber = phoneNumber;
					String msg = smsMessage.getDisplayMessageBody();

					Log.i("Sms Receiver", "Sender Number :" + senderNumber
							+ ", Message :" + msg);

					Toast.makeText(
							context,
							"Sender Number :" + senderNumber + ", Message :"
									+ msg, Toast.LENGTH_SHORT).show();
					
					if (true) {
						//((MobileNumber)context).check();
					}

				}// end for loop
			}// bundle null

		} catch (Exception e) {
			
			e.printStackTrace();

		}

	}

}

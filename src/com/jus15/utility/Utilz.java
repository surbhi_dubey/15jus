package com.jus15.utility;

import android.content.Context;
import android.net.ConnectivityManager;

public class Utilz {

	public boolean isInternetConnected(Context c) {
		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}
}

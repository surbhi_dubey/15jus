package com.jus15;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jus15.json.AsyncTaskJUS15;
import com.jus15.json.AsyncTaskJUS15.AsyncTaskPurpose;
import com.jus15.json.JSONMaker;
import com.jus15.ui.RippleView;
import com.jus15.utility.Constraint;

public class VerificationCode extends Activity implements OnClickListener {

	EditText editText_verification_code;

	final int WaitTimer = 1000;

	Context context;

	String userVerificationCode = "", device_id, phone, userName, intentOTP;

	String smsOTP;

	String OtpCOde = "";

	RelativeLayout progressWheelLayout_Activation;

	ImageView imageCheck;

	boolean flag = false;
	Handler handler;
	Runnable runnable;

	Timer t;
	private int questionDisplayTime = 20;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.verification_code);

		context = this;

		((RippleView) findViewById(R.id.rippleViewVerification))
				.setOnClickListener(this);

		try {
			progressWheelLayout_Activation = (RelativeLayout) findViewById(R.id.progressWheelLayout_Activation);
		} catch (Exception exception) {
		}

		editText_verification_code = (EditText) findViewById(R.id.varification_code);

		imageCheck = (ImageView) findViewById(R.id.imageCheck);

		Intent i = getIntent();
		Bundle bundle = i.getExtras();
		intentOTP = bundle.getString("OTP");
		phone = bundle.getString("MobileNo");
		userName = bundle.getString("UserName");

		// Getting Mobile Id
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		device_id = telephonyManager.getDeviceId();

		// Execute some code after 20 seconds have passed

		// handler = new Handler();
		// handler.postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		//
		// progressWheelLayout_Activation.setVisibility(View.GONE);
		// OtpCOde = Constraint.OTP;
		// if (OtpCOde != null) {
		//
		// imageCheck.setVisibility(View.VISIBLE);
		// editText_verification_code.setText(OtpCOde);
		//
		// }
		//
		// }
		// }, WaitTimer);

		// handler = new Handler();
		// runnable = new Runnable() {
		// @Override
		// public void run() {
		// progressWheelLayout_Activation.setVisibility(View.GONE);
		// Toast.makeText(getApplicationContext(), "After...",
		// Toast.LENGTH_LONG).show();
		//
		// }
		// };
		// handler.postDelayed(runnable, WaitTimer);

		/* check after 20 sec otp is not receive then fill by user */
		editText_verification_code.setFocusable(false);
		editText_verification_code.setFocusableInTouchMode(false);
		startTimer();

		editText_verification_code.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				if (editText_verification_code.getText().toString()
						.equals(Constraint.OTP)) {

					imageCheck.setVisibility(View.VISIBLE);
				} else {
					Toast.makeText(context, "WRONG OTP", 2000).show();
					imageCheck.setVisibility(View.GONE);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

				// if (userVerificationCode.equals(intentOTP)) {
				// progressWheelLayout_Activation.setVisibility(View.GONE);
				// imageCheck.setVisibility(View.VISIBLE);
				//
				// } else {
				// // Toast.makeText(context, "OTP is not Matching",
				// // Toast.LENGTH_LONG).show();
				// }
			}
		});

		// Toast.makeText(context, "OTP is not Matching",
		// Toast.LENGTH_LONG).show();

	}

	@Override
	public void onClick(View v) {

		userVerificationCode = editText_verification_code.getText().toString();

		registrationApi();

	}

	private void startTimer() {
		t = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (Constraint.OTP.length() > 0) {

							editText_verification_code.setText(Constraint.OTP
									.toString());
							Toast.makeText(context, "found", 2000).show();
							progressWheelLayout_Activation
									.setVisibility(View.GONE);

							if (t != null) {
								t.cancel();
								t.purge();
							}

						} else if (questionDisplayTime > 0) {
							Toast.makeText(context, "running", 2000).show();
							questionDisplayTime -= 1;
						} else {
							progressWheelLayout_Activation
									.setVisibility(View.GONE);
							editText_verification_code.setFocusable(true);
							editText_verification_code
									.setFocusableInTouchMode(true);

							if (t != null) {
								t.cancel();
								t.purge();
							}

						}
					}
				});
			}
		};

		t.scheduleAtFixedRate(task, 0, 1000);
	}

	private void registrationApi() {

		// Calling Asynctask Task Class...............
		ArrayList<HashMap> arrayJsonRequired = new ArrayList<HashMap>();

		AsyncTaskJUS15 asyncTaskClass = new AsyncTaskJUS15(context,
				AsyncTaskPurpose.registration);

		HashMap<String, String> namemap = new HashMap<String, String>();

		namemap.put("JSONKEYNAME", "name");
		namemap.put("JSONKEYNAMEVALUE", userName);

		HashMap<String, String> mobilemap = new HashMap<String, String>();

		mobilemap.put("JSONKEYNAME", "mobile");
		mobilemap.put("JSONKEYNAMEVALUE", phone);

		HashMap<String, String> deviceidmap = new HashMap<String, String>();

		deviceidmap.put("JSONKEYNAME", "device_id");
		deviceidmap.put("JSONKEYNAMEVALUE", device_id);

		arrayJsonRequired.add(namemap);
		arrayJsonRequired.add(mobilemap);
		arrayJsonRequired.add(deviceidmap);

		JSONMaker jsonmaker = new JSONMaker(arrayJsonRequired);
		String jsonstring = jsonmaker.createJson();
		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("user_detail", jsonstring);

		asyncTaskClass.execute(hashMap);
	}

	public void checkResult(int status, String msg) {

		if (status == 1) {

			Intent intent = new Intent(VerificationCode.this,
					LandingScreen.class);
			intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK
					| intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_in,
					R.anim.slide_out);

			/* .....get shared preference private mode..... */
			SharedPreferences pref = getApplicationContext()
					.getSharedPreferences("option", MODE_PRIVATE);

			/* ......edit SharedPreference........ */
			Editor editor = pref.edit();
			editor.putString("mobile", phone);
			editor.putString("deviceId", device_id);

			editor.commit(); // end shared preference

			Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

		} else {

			Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.back_slide_in,
				R.anim.back_slide_out);
	}


	
}

package com.jus15.location;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.jus15.LandingScreen;
import com.jus15.R;
import com.jus15.database.DbHelper;
import com.jus15.json.AsyncTaskJUS15;
import com.jus15.json.AsyncTaskJUS15.AsyncTaskPurpose;
import com.jus15.json.JSONMakerArray;
import com.jus15.utility.Constraint;
import com.jus15.utility.Utilz;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class GPSTracker implements LocationListener {

	private Context mContext;

	int batterylevel;

	DbHelper db;
	int i = 0;
	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	// flag for GPS status
	boolean canGetLocation = false;

	Location location; // location

	String latitude; // latitude
	String longitude; // longitude

	int status;

	String phone_status, timeStamp;

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public Location getLocation(Context context) {

		this.mContext = context;

		try {
			locationManager = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
			} else {

				this.canGetLocation = true;
				if (isNetworkEnabled) {

					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

					Log.d("Network", "Network");

					if (locationManager != null) {

						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

						if (location != null) {

							latitude = Double.toString(location.getLatitude());
							longitude = Double
									.toString(location.getLongitude());

						}
					}
				}

				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {

					if (location == null) {

						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

						Log.d("GPS Enabled", "GPS Enabled");

						if (locationManager != null) {

							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);

							if (location != null) {

								latitude = Double.toString(location
										.getLatitude());
								longitude = Double.toString(location
										.getLongitude());

							}
						}
					}
				}

			}

			Calendar calendar = Calendar.getInstance();

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");

			timeStamp = dateFormat.format(calendar.getTime());
			mContext = context;

			SharedPreferences pref = context.getSharedPreferences("option",
					Context.MODE_PRIVATE);
			phone_status = pref.getString("mobile", phone_status);
			// phone_status = "0909090909";
			// phoneStatus();
			// Toast.makeText(mContext, "phone" + phone_status + "\n" +
			// "Battery Level" + batterylevel + "\n" + "Lattitude=" + latitude
			// + "" + " Longitude= " + longitude + ""
			// + "\n TimeStamp= " + timeStamp, Toast.LENGTH_LONG)
			// .show();

			batterylevel = Constraint.battery_level;

			String deviceId = pref.getString("deviceId", null);

			if (latitude == null && longitude == null) {
				notificatioGps(mContext);

			} else {

				Toast.makeText(
						mContext,
						"phone" + phone_status + "\n" + "Battery Level"	+ batterylevel + "\n" + "Lattitude=" + latitude
								+ "" + " Longitude= " + longitude + ""
								+ "\n TimeStamp= " + timeStamp, 2000).show();
				
				insertData(context, phone_status,Integer.toString(batterylevel), latitude, longitude,timeStamp, deviceId);
				
				
//				db = new DbHelper(context);
//				db.createDb(false);
//				this.mContext = context;
//				status = 0;
//				db.addData(phone_status, Integer.toString(batterylevel),
//						latitude, longitude, timeStamp, status, deviceId);

				Utilz utilz = new Utilz();

				if (utilz.isInternetConnected(context)) {
					getData(context);
				} else {
					notificationInternet();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
		}
		return location;
	}

	public void updateService(Context context, int Batterylevel, String latti,
			String longi, String timeStamp, String phone_No) {
		// Calling Asynctask Task Class...............
		ArrayList<HashMap> arrayJsonRequired = new ArrayList<HashMap>();

		AsyncTaskJUS15 asyncTaskClass = new AsyncTaskJUS15(mContext,
				AsyncTaskPurpose.phoneStatus);

		HashMap<String, String> phonemap = new HashMap<String, String>();

		phonemap.put("JSONKEYNAME", "phone");
		phonemap.put("JSONKEYNAMEVALUE", phone_No);

		HashMap<String, String> batterymap = new HashMap<String, String>();

		batterymap.put("JSONKEYNAME", "battery_percentage");
		batterymap.put("JSONKEYNAMEVALUE", Integer.toString(batterylevel));

		HashMap<String, String> latitudemap = new HashMap<String, String>();

		latitudemap.put("JSONKEYNAME", "latitude");
		latitudemap.put("JSONKEYNAMEVALUE", latitude);

		HashMap<String, String> longitudemap = new HashMap<String, String>();

		longitudemap.put("JSONKEYNAME", "longitude");
		longitudemap.put("JSONKEYNAMEVALUE", longitude);

		HashMap<String, String> timemap = new HashMap<String, String>();

		timemap.put("JSONKEYNAME", "time");
		timemap.put("JSONKEYNAMEVALUE", timeStamp);

		arrayJsonRequired.add(phonemap);
		arrayJsonRequired.add(batterymap);
		arrayJsonRequired.add(latitudemap);
		arrayJsonRequired.add(longitudemap);
		arrayJsonRequired.add(timemap);

		JSONMakerArray jsonmaker = new JSONMakerArray(arrayJsonRequired);
		String jsonstring = jsonmaker.createJson();

		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("status", jsonstring);

		Utilz utilz = new Utilz();
		if (!utilz.isInternetConnected(context)) {
			notificationInternet();
		} else

			asyncTaskClass.execute(hashMap);
	}

	public void stopUsingGPS() {

		if (locationManager != null) {

			locationManager.removeUpdates(GPSTracker.this);
		}
	}

	public String getLatitude() {

		if (location != null) {

			latitude = Double.toString(location.getLatitude());
		}

		// return latitude
		return latitude;
	}

	public String getLongitude() {

		if (location != null) {

			longitude = Double.toString(location.getLongitude());
		}

		// return longitude
		return longitude;
	}

	public boolean canGetLocation() {

		return this.canGetLocation;
	}

	@Override
	public void onLocationChanged(Location location) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	public void notificatioGps(Context context) {

		Notification.Builder builder = new Notification.Builder(
				LandingScreen.context);
		builder.setSmallIcon(R.drawable.icon);
		builder.setContentTitle("Please switch on your GPS");
		builder.setContentText("To get the 15JUS battery pack at the right location.");
		Notification notification = builder.build();
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		NotificationManager notificationmanager = (NotificationManager) LandingScreen.context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationmanager.notify(01, notification);

	}

	public void notificationInternet() {

		Notification.Builder builder = new Notification.Builder(
				LandingScreen.context);

		builder.setSmallIcon(R.drawable.icon);

		builder.setContentTitle("Please switch on your Wifi or Data connection");

		builder.setContentText("To get the 15JUS battery pack when you need it");

		Notification notification = builder.build();

		notification.flags = Notification.FLAG_AUTO_CANCEL;

		NotificationManager notificationmanager = (NotificationManager) LandingScreen.context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		notificationmanager.notify(01, notification);

	}

	public void insertData(Context context, String phone,
			String batteryPercent, String lattitude, String longitude,
			String time, String deviceId) throws IOException {

		db = new DbHelper(context);
		db.createDb(false);
		this.mContext = context;
		status = 0;
		db.addData(phone, batteryPercent, lattitude, longitude, time, status,
				deviceId);
	//	getData(mContext);

	}

	public void getData(Context context) {

		Utilz utilz = new Utilz();
		utilz.isInternetConnected(context);

		String jsondata = db.fetchOfflineData();

		if (jsondata != null) {

			if (jsondata.trim().length() > 0) {
				AsyncTaskJUS15 asynTaskBolt = new AsyncTaskJUS15(context,
						AsyncTaskPurpose.phoneStatus);

				HashMap<String, String> parm = new HashMap<String, String>();
				parm.put("status", jsondata);

				asynTaskBolt.execute(parm);
			}
		}

	}

	public void checkResult(int status, String msg) {

		if (status == 1) {

			Toast.makeText(LandingScreen.context, msg, Toast.LENGTH_LONG)
					.show();

			db = new DbHelper(LandingScreen.context);
			db.createDb(false);
			db.deletecontact();

		} else {

			Toast.makeText(LandingScreen.context, msg, Toast.LENGTH_LONG)
					.show();

		}
	}

}